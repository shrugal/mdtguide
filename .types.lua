---@diagnostic disable: duplicate-set-field
---@meta

---
-- This class contains EmmyLua annotations to help
-- IDEs work with some external classes and types
---

---@class MythicDungeonTools
---@field scaleMultiplier number[]
---@field mapPOIs MDTMapPOI[][][]
---@field dungeonEnemies MDTEnemy[][]
---@field zoneIdToDungeonIdx number[]
---@field main_frame MDTMainFrame
---@field mapPanelFrame Frame
---@field EnemyInfoFrame MDTEnemyInfoFrame
---@field pullTooltip GameTooltip
---@field dungeonSelectionToIndex number[][]
---@field GetDB fun(self: self): MDTDB
---@field ShowInterface fun(self: self)
---@field HideInterface fun(self: self)
---@field StartScaling fun(self: self)
---@field GetScale fun(self: self): number
---@field SetScale fun(self: self, scale: number)
---@field UpdateMap fun(self: self, a?: boolean, b?: boolean, c?: boolean)
---@field DrawAllHulls fun(self: self)
---@field Minimize fun(self: self)
---@field UpdateEnemyInfoFrame fun(self: self)
---@field ZoomMap fun(self: self, zoom: number)
---@field GetCurrentSubLevel fun(self: self): number, number
---@field SetCurrentSubLevel fun(self: self, subLevel: number)
---@field DungeonEnemies_UpdateSelected fun(self: self)
---@field GetCurrentPull fun(self: self): number
---@field GetCurrentPreset fun(self: self): MDTPreset
---@field CountForces fun(self: self, pull?: number, a?: boolean): number
---@field FormatEnemyForces fun(self: self, forcesCount: number): string
---@field SetSelectionToPull fun(self: self, pull: number)
---@field IsCloneIncluded fun(self: self, enemyId: number, cloneId: number): boolean
---@field GetBlip fun(self: self, enemyId: number, cloneId: number): MDTBlip?
---@field GetNumDungeons fun(self: self): number
MDT = nil

---@class MDTDB
---@field devMode boolean
---@field nonFullscreenScale number
---@field selectedDungeonList number

---@class MDTPreset
---@field value MDTPresetValue

---@class MDTPresetValue
---@field currentPull number
---@field selection number[]
---@field pulls MDTPull[]

---@alias MDTPull number[][]

---@class MDTPosition
---@field sublevel number
---@field x number
---@field y number
---@field g? number

---@class MDTMapPOI: MDTPosition
---@field type string
---@field connectionIndex number

---@class MDTEnemy
---@field id number
---@field name? string
---@field instanceID number
---@field isBoss boolean
---@field encounterID? number
---@field count number
---@field clones MDTClone[]

---@class MDTClone: MDTPosition

---@class MDTMainFrame: Frame
---@field resizer Frame
---@field topPanel AceGUIWidget
---@field bottomPanel AceGUIWidget
---@field sidePanel MDTSidePanel
---@field mapPanelFrame Frame
---@field scrollFrame ScrollFrame
---@field toolbar MDTToolbar
---@field DungeonSelectionGroup AceGUIContainer
---@field topPanelLogo AceGUIIcon
---@field closeButton Button
---@field maximizeButton AceGUIButton
---@field HelpButton AceGUIButton
---@field bottomPanelString FontString

---@class MDTSidePanel: AceGUIWidget
---@field PullButtonScrollGroup AceGUIWidget
---@field pullButtonsScrollFrame AceGUIScrollFrame
---@field WidgetGroup MDTSidePanelWidgetGroup
---@field ProgressBar AceGUIWidget
---@field newPullButtons AceGUIButton[]

---@class MDTSidePanelWidgetGroup: AceGUIWidget
---@field PresetDropDown AceGUIDropdown

---@class MDTToolbar: AceGUIWidget
---@field toggleButton Button

---@class MDTEnemyInfoFrame: AceGUIWindow
---@field enemyDataContainer MDTEnemyDataContainer
---@field spellScroll AceGUIWidget
---@field spellScrollContainer AceGUIContainer

---@class MDTEnemyDataContainer: AceGUIContainer
---@field stealthCheckBox AceGUICheckBox
---@field stealthDetectCheckBox AceGUICheckBox

---@class MDTBlip: Frame
---@field texture_SelectedHighlight Texture
---@field texture_Portrait Texture

---@type Frame
MDTScrollFrame = nil

-- WoW methods

---@class stringlib
---@field split fun(delimiter: string, str: string, pieces?: integer): ...: string
---@field trim fun(str: string): string

---@class string
---@field split fun(self: self, delimiter: string, pieces?: integer): ...: string
---@field trim fun(self: self): self

---@class MaximizeMinimizeButtonFrame: Frame
---@field SetOnMaximizedCallback function
---@field SetOnMinimizedCallback function

---@class IconButton: Button

---@class SquareIconButton: IconButton

---@class C_Addons
---@field GetAddOnMetadata fun(addonId: string | number, field: string): string
C_AddOns = nil